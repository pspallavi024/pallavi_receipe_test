package Pallavi_RecipesBook;

public class Recipe {
        private  int Id;
        private  String Title;
        private String Image;
        private  String[] Ingredients;
        private  String Direction;
    private int id;

    public Recipe(int Id,String Title, String Image, String[] Ingredients, String Direction)
        {
            this.Id = Id;
            this.Title = Title;
            this.Image = Image;
            this.Ingredients = Ingredients;
            this.Direction = Direction;
        }

        public void setRecipe (Recipe recipe)
        {
            this.Id = recipe.getId();
            this.Title = recipe.getTitle();
            this.Image = recipe.getImage();
            this.Ingredients = recipe.getIngredients();
            this.Direction = recipe.getDirection();
        }


        public int getId ()
        {
            return Id;
        }

        public String getTitle ()
        {
            return Title;
        }

        public String getImage ()
        {
            return Image;
        }

        public String[] getIngredients ()
        {
            return Ingredients ;
        }

        public String getDirection()
        {
            return Direction;
        }

    public void setId(int id) {
        this.id = id;
    }
}

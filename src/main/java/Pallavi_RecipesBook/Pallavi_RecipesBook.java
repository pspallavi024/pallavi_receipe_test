package Pallavi_RecipesBook;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.controlsfx.control.CheckComboBox;

import java.io.*;
import java.net.MalformedURLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.sql.*;
import java.util.logging.*;

public class Pallavi_RecipesBook extends Application {
    private static final Logger logger = Logger.getLogger(Pallavi_RecipesBook.class.getName());
    private static final String[] SAMPLE_NAME_DATA = { "Amazing Muffin Cups", "Authentic Mexican Tacos", "Apple Pie", "Baked Brunch Omelet", "Breakfast Pies", "Breakfast Pizza"};
    //declaring global variables that can be accessed by all methods
    List<String> allSelected = new ArrayList<>() ;
    RecipeBook allrecipe = new RecipeBook();
    Label[] numberIngredients = new Label [allrecipe.getRecipeBook().getRecipes().length];
    Label[] listRecipe = new Label [allrecipe.getRecipeBook().getRecipes().length];
    CheckBox[] box = new CheckBox [listRecipe.length];

    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) throws FileNotFoundException, InterruptedException, MalformedURLException {
        final ListView<String> nameView = new ListView();
        // Create a border pane
        BorderPane pane = new BorderPane();

        pane.setTop(getHBox());
        //thread for title
        starter();

        //The Ingredient menu
        pane.setLeft(getVBox());

        //Recipe list
        ScrollPane AllRecipe = new ScrollPane (getGridPane());
        pane.setCenter(AllRecipe);

        SplitPane splitPane = new SplitPane();
        splitPane.getItems().addAll(pane, recipePreview);

        // Import and play intro video
        File file = new File("src/main/resources/img/new.mp4");
        Media media = new Media(file.toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        MediaView view = new MediaView();

        DoubleProperty width = view.fitWidthProperty();
        DoubleProperty height = view.fitHeightProperty();
        width.bind(Bindings.selectDouble(view.sceneProperty(), "width"));
        height.bind(Bindings.selectDouble(view.sceneProperty(), "height"));
        view.setPreserveRatio(true);

        Group root = new Group(view);
        Scene scene1 = new Scene(root);


        primaryStage.setTitle("Pallavi RecipesBook"); // Set the stage title
        primaryStage.setScene(scene1); // Place the scene in the stage
        primaryStage.show(); // Display the stage
        primaryStage.setFullScreen(true);

        view.setMediaPlayer(player);
        player.play();
        player.setOnEndOfMedia(() -> {

            Scene scene = new Scene(splitPane, 1800, 900);

            primaryStage.setScene(scene);
            primaryStage.setFullScreen(true);

            view.setVisible(false);
        });

        //list of missing ingredients for selected recipes
        List <String> missingList = new ArrayList<>();

        shoppingList.setOnAction((e)-> {

            missingList.clear();

            for (int i = 0; i < listRecipe.length; i++)
            {
                //if recipe is selected
                if(box[i].isSelected())
                {
                    //compare each ingriedients of selected recipe with slected ingredients
                    for (String ingredient : allrecipe.getRecipeBook().getRecipes()[i].getIngredients())
                    {
                        boolean isMissing = true;
                        for (int j = 0; j< allSelected.size(); j++)
                        {
                            if (allSelected.get(j).equalsIgnoreCase(ingredient))
                            {
                                isMissing = false ;
                                break;
                            }
                        }
                        //if ingriedients of selected recipe is not in the slected ingredients mark as missing
                        if (isMissing == true && !missingList.contains(ingredient))
                            missingList.add(ingredient);

                    }
                }
            }

            Stage dialog = new Stage();
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(primaryStage);
            VBox dialogVbox = new VBox(10);

            BackgroundImage backgroundimage2 = new BackgroundImage(new Image("img/Pallavi-Recipe.png", 900,900, true, true),
                    BackgroundRepeat.ROUND,
                    BackgroundRepeat.ROUND,
                    BackgroundPosition.CENTER,
                    new  BackgroundSize(100, 100, true, true,true, true));
            dialogVbox.setBackground(new Background(backgroundimage2));

            Text missingTitle = new Text ("Missing Ingredients");
            missingTitle.setFont(Font.font("Papyrus", FontWeight.BOLD, 20));
            dialogVbox.getChildren().add(missingTitle);

            for(int i=0; i < missingList.size(); i++)
            {
                //print each missing ingridients
                dialogVbox.getChildren().add(new Text (missingList.get(i)));
            }

            Button save = new Button("Save shopping list");
            //creat a file with all missing ingridients and save the file in the computer
            save.setOnAction((f)->{
                try
                {
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(".\\shopping_list.txt")))
                    {
                        for (int i =0; i < missingList.size(); ++i)
                        {
                            writer.write(missingList.get(i)+"\n");
                        }
                    }

                }
                catch (IOException ex)
                {
                    Logger.getLogger(Pallavi_RecipesBook.class.getName()).log(Level.SEVERE, null, ex);
                }

            });
            dialogVbox.getChildren().add(save);

            Scene dialogScene = new Scene(dialogVbox);
            dialog.setScene(dialogScene);
            dialog.show();

        });

        primaryStage.setOnCloseRequest((WindowEvent t) -> {
            Platform.exit();
            System.exit(0);
        });
        final Button shoppingList = new Button("Fetch names from the database");
        shoppingList.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                fetchNamesFromDatabaseToListView(nameView);
            }
        });
    }

    //Multithread starter
    public void starter()
    {
        Multip thread =  new Multip();
        thread.start();
    }

    //Multithread
    public class Multip extends Thread
    {
        @Override
        @SuppressWarnings("SleepWhileInLoop")
        public void run()
        {
            try
            {
                while(true)
                {
                    text.setText(xxx);
                    char x = xxx.charAt(0);
                    String xx = xxx.substring(1);
                    xxx=xx + x;
                    Thread.sleep(300);
                }
            }
            catch (InterruptedException e)
            {
                // Throwing an exception
                System.out.println ("Exception is caught");
            }
        }
    }

    //Title used in multi and HBox
    Text text=new Text();
    String xxx = " Pallavi_Recipe Book Test...";

    //Title
    private HBox getHBox() throws FileNotFoundException
    {
        HBox hBox = new HBox();
        hBox.setPadding(new Insets(5, 5, 5, 5));
        hBox.setStyle("-fx-background-color: green");

        text.setFont(Font.font("Papyrus", FontWeight.SEMI_BOLD, 70));
        text.setFill(Color.SIENNA);

        HBox qu = new HBox(text);
        qu.setAlignment(Pos.TOP_RIGHT);

        hBox.getChildren().add(qu);

        return hBox;
    }

    Button shoppingList = new Button ("Save the Recipe");

    private VBox getVBox()
    {
        VBox vBox = new VBox(15);
        vBox.setPadding(new Insets(15, 5, 5, 5));
        vBox.setMaxWidth(200);
        vBox.getChildren().add(new Label("Select your ingredients"));


        Label[] catagory = {new Label("Dairy"), new Label("Vegetable"), new Label("Fruit"), new Label("Grains Products"),new Label("Breads/Crusts")};


        final ObservableList<String> strings = FXCollections.observableArrayList();
        strings.addAll("Butter","Buttermilk","Cheddar cheese","Cottage Cheese","Colby-Monterey Jack cheese","Cream cheese","Egg","Egg yolks","Egg whites","Evaporated milk","Half-and-half cream","Heavy cream","Parmesan cheese","Margarine","Mexican cheese blend","Milk","Monterey Jack cheese","Mexican blend cheese","Sharp cheddar cheese");

        final ObservableList<String> strings1 = FXCollections.observableArrayList();
        strings1.addAll("Bell pepper","Broccoli","Chives","Green onions","Hash brown potatoes","Hunt's(R) Diced Tomatoes","Jalapeno pepper","Garlic","Green Onions","Mushrooms","Onion","Parsley","Potatoes","Red Potatoes","Red Bell Pepper","Sweet onion");

        final ObservableList<String> strings2 = FXCollections.observableArrayList();
        strings2.addAll("Apple","Blueberries","Cranberries","Dried figs","Lemon juice","Orange juice","Orange liqueur","Raisins","Hazelnuts","Raisins");

        final ObservableList<String> strings3 = FXCollections.observableArrayList();
        strings3.addAll("All-purpose flour","Almonds","Flour","Oats","Pecans","Peanut butter","Self-rising flour","Sunflower seeds","Toasted hazelnuts","Walnuts","Wheat germ");

        final ObservableList<String> strings4 = FXCollections.observableArrayList();
        strings4.addAll("Bread","Biscuit dough","Biscuit baking mix","Crescent rolls","Corn tortillas","Deep dish frozen pie crust","English muffins","Flour tortillas ","French bread","Pie crust","Southern-style flaky refrigerated biscuits","White bread");


        // Create the CheckComboBox with the Ingredients
        final CheckComboBox<String> checkComboBox = new CheckComboBox<>(strings);
        final CheckComboBox<String> checkComboBox1 = new CheckComboBox<>(strings1);
        final CheckComboBox<String> checkComboBox2 = new CheckComboBox<>(strings2);
        final CheckComboBox<String> checkComboBox3 = new CheckComboBox<>(strings3);
        final CheckComboBox<String> checkComboBox4 = new CheckComboBox<>(strings4);


        for(int i=0; i<catagory.length; i++)
        {
            VBox.setMargin(catagory[i], new Insets(0, 0, 0, 15));
            vBox.getChildren().add(catagory[i]);

            switch(i)
            {
                case(0):
                    vBox.getChildren().add(checkComboBox);
                    break;
                case(1):
                    vBox.getChildren().add(checkComboBox1);
                    break;
                case(2):
                    vBox.getChildren().add(checkComboBox2);
                    break;
                case(3):
                    vBox.getChildren().add(checkComboBox3);
                    break;
                case(4):
                    vBox.getChildren().add(checkComboBox4);
                    break;
            }
        }

        Button go = new Button ("Find Recipes");
        VBox.setMargin(go, new Insets(50, 0, 50, 50));
        go.setShape(new Circle(10));
        vBox.getChildren().add(go);

        vBox.getChildren().add(shoppingList);

        go.setOnAction((e)->
        {

            allSelected.clear();

            allSelected.addAll((List) checkComboBox.getCheckModel().getCheckedItems());
            allSelected.addAll((List) checkComboBox1.getCheckModel().getCheckedItems());
            allSelected.addAll((List) checkComboBox2.getCheckModel().getCheckedItems());
            allSelected.addAll((List) checkComboBox3.getCheckModel().getCheckedItems());
            allSelected.addAll((List) checkComboBox4.getCheckModel().getCheckedItems());

            //count number of ingredients
            coutMissing(allSelected);
            //reorder recipe in order of ingredients
            reorderRecipes();
        });

        return vBox;
    }



    Text showRecipe = new Text();
    FlowPane recipePreview = new FlowPane();

    private GridPane getGridPane() throws FileNotFoundException, MalformedURLException {
        GridPane gridPane = new GridPane();
        gridPane.setMinSize(400, 200);
        gridPane.setPadding(new Insets(10, 10, 10, 10));


        //Setting the vertical and horizontal gaps between the columns
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setStyle("-fx-background-color: pinksilk");

        //calls function that list all recipes
        listAllRecipes();

        //gets the image of each recipe and intializies missing ingredients
        for(int i=0; i < allrecipe.getRecipeBook().getRecipes().length; i++ )
        {
            ImageView iw3 =new ImageView(new Image(allrecipe.getRecipeBook().getRecipes()[i].getImage())) ;
            iw3.setFitHeight(200);
            iw3.setFitWidth(200);

            numberIngredients[i] = new Label("# List of Ingredients",iw3 );
        }


        //shows recipe discreption when clicked
        recipePreview.getChildren().add(showRecipe);
        showRecipe.setText("Recipe Information");
        showRecipe.setFont(Font.font("Papyrus", FontWeight.BLACK, 18));
        showRecipe.setWrappingWidth(450);

        numberIngredients[0].setOnMouseClicked(e-> { showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[0].getDirection());});
        numberIngredients[1].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[1].getDirection());});
        numberIngredients[2].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[2].getDirection());});
        numberIngredients[3].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[3].getDirection());});
        numberIngredients[4].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[4].getDirection());});
        numberIngredients[5].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[5].getDirection());});
        numberIngredients[6].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[6].getDirection());});
        numberIngredients[7].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[7].getDirection());});
        numberIngredients[8].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[8].getDirection());});
        numberIngredients[9].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[9].getDirection());});
        numberIngredients[10].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[10].getDirection());});
        numberIngredients[11].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[11].getDirection());});
        numberIngredients[12].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[12].getDirection());});
        numberIngredients[13].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[13].getDirection());});
        numberIngredients[14].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[14].getDirection());});
        numberIngredients[15].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[15].getDirection());});
        numberIngredients[16].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[16].getDirection());});
        numberIngredients[17].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[17].getDirection());});
        numberIngredients[18].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[18].getDirection());});
        numberIngredients[19].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[19].getDirection());});
        numberIngredients[20].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[20].getDirection());});
        numberIngredients[21].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[21].getDirection());});
        numberIngredients[22].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[22].getDirection());});
        numberIngredients[23].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[23].getDirection());});
        numberIngredients[24].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[24].getDirection());});
        numberIngredients[25].setOnMouseClicked(e-> {showRecipe.setText(allrecipe.getRecipeBook().getRecipes()[25].getDirection());});

        for (int i = 0; i < listRecipe.length; i++)
        {
            final CheckBox box1 = box[i] = new CheckBox(listRecipe[i].getText());
        }



        int j=0, k=0, l=1;
        for(int i=0;i<listRecipe.length;i++)
        {
            numberIngredients[i].setContentDisplay(ContentDisplay.TOP);
            numberIngredients[i].setStyle("-fx-border-color: brown;");

            if(i%5==0 && i!=0)
            {    k+=2; l+=2; j=0;}

            gridPane.add( box[i], j, k);
            gridPane.add(numberIngredients[i], j, l);

            j++;
        }

        return gridPane;
    }


    int [] Ingredients = new int [allrecipe.getRecipeBook().getRecipes().length];

    public void coutMissing(List<String> allSelected)
    {
        for(int l=0; l<Ingredients.length;l++)
        {Ingredients[l]=0;}

        for(int i=0; i< allrecipe.getRecipeBook().getRecipes().length ; i++)
        {
            for (String ingredient : allrecipe.getRecipeBook().getRecipes()[i].getIngredients())
            {
                boolean isMissing = true;

                for (int j = 0; j< allSelected.size(); j++)
                {
                    if (allSelected.get(j).equalsIgnoreCase(ingredient))
                    {
                        isMissing = false ;
                        break;
                    }
                }
                if( isMissing == true)
                    ++Ingredients[i];
            }
        }
    }

    public void reorderRecipes()
    {
        RecipeBook temp = new RecipeBook();
        int[] minimumIndexes = new int[Ingredients.length];
        int[] sortedArray = Ingredients.clone();

        Arrays.sort(sortedArray);
        Set<Integer> savedIndexes = new HashSet<>();

        for (int index = 0; index < Ingredients.length; index++)
        {
            int minIndex = 0;
            // Add the index in ascending order, we need to keep the indexes already
            // saved, so won't miss indexes from repeted values
            for (int number : Ingredients) {
                if (number == sortedArray[index] && !savedIndexes.contains(minIndex)) {
                    savedIndexes.add(minIndex);
                    minimumIndexes[index] = minIndex;
                    break;
                }
                minIndex++;
            }
        }

        for(int i=0; i< allrecipe.getRecipeBook().getRecipes().length; i++)
        {
            temp.getRecipeBook().getRecipes()[i].setRecipe(allrecipe.getRecipeBook().getRecipes()[minimumIndexes[i]]);
            numberIngredients[i].setText("Number of ingredients - "+Integer.toString(sortedArray[i]));

            ImageView iw3 =new ImageView(new Image(temp.getRecipeBook().getRecipes()[i].getImage())) ;
            iw3.setFitHeight(200);
            iw3.setFitWidth(200);
            numberIngredients[i].setGraphic(iw3);
        }
        allrecipe.setRecipeBook(temp);
        listAllRecipes();
        for(int i=0; i< box.length ; i++) {
            box[i].setText(listRecipe[i].getText());
        }
    }

    public void listAllRecipes() {
        for(int i=0; i< allrecipe.getRecipeBook().getRecipes().length; i++) {
            listRecipe[i] = new Label(allrecipe.getRecipeBook().getRecipes()[i].getTitle());
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void fetchNamesFromDatabaseToListView(ListView listView) {
        try (Connection con = getConnection()) {
            if (!schemaExists(con)) {
                createSchema(con);
                populateDatabase(con);
            }
            listView.setItems(fetchNames(con));
        } catch (SQLException | ClassNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        logger.info("Getting a database connection");
        Class.forName("org.h2.Driver");
        return DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
    }

    private void createSchema(Connection con) throws SQLException {
        logger.info("Creating schema");
        Statement st = con.createStatement();
        String table = "create table recipe(id integer, Recipename varchar(64))";
        st.executeUpdate(table);
        logger.info("Created schema");
    }

    private void populateDatabase(Connection con) throws SQLException {
        logger.info("Populating database");
        Statement st = con.createStatement();
        int i = 1;
        for (String name: SAMPLE_NAME_DATA) {
            st.executeUpdate("insert into recipe values(i,'" + name + "')");
            i++;
        }
        logger.info("Populated database");
    }

    private boolean schemaExists(Connection con) {
        logger.info("Checking for Schema existence");
        try {
            Statement st = con.createStatement();
            st.executeQuery("select count(*) from recipe");
            logger.info("Schema exists");
        } catch (SQLException ex) {
            logger.info("Existing DB not found will create a new one");
            return false;
        }

        return true;
    }

    private ObservableList<String> fetchNames(Connection con) throws SQLException {
        logger.info("Fetching names from database");
        ObservableList<String> names = FXCollections.observableArrayList();

        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select name from recipe");
        while (rs.next()) {
            names.add(rs.getString("name"));
        }

        logger.info("Found " + names.size() + " names");

        return names;
    }
}
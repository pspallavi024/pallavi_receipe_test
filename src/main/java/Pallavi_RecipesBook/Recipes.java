package Pallavi_RecipesBook;

public class Recipes {
    private Recipe[] recipes;
    public Recipes( Recipe[] recipe)
    {
        this.recipes = recipe;
    }
    public Recipe[] getRecipes()
    {
        return recipes;
    }
    public void setRecipes( Recipe[] recipes)
        {
            this.recipes = recipes;
        }
}

package repository;


import Pallavi_RecipesBook.Recipe;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeRepository extends CrudRepository<Recipe, Integer> {
	
	public List<Recipe>  findAll();
}

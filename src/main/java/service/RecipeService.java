package service;



import Pallavi_RecipesBook.Recipe;

import java.util.List;

public interface RecipeService {
	
	public List<Recipe> findAll();
	
	public Recipe findById(int id);
	
	public Recipe update(Recipe recipe);
	
	public Recipe create(Recipe recipe);
	
	public void deleteRecipe(int id);

}
